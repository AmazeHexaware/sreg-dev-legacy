﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Data;

public partial class admin_Ragistrations : System.Web.UI.Page
{
    string password = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Session["RagistrationId"] != null && Session["Edit"] != null)
            {
                //update
                FillUpdate();
            }
            else
            {
                Clear();
            }

        }
    }
    void Clear()
    {
        rbtnPref_Language.Text = "";
        rbtnCategory.SelectedValue = "";
        txtPassword.Text = "";
        txtRagistrationId.Text = "";
        txtBankNo.Text = "";
        
        txtS_DD.Text = "";
        txtS_MM.Text = "";
        txtS_YY.Text = "";
        txtSalaryAmount.Text = "";
        txtBankName.Text = "";
        txtName.Text = "";
        txtFName.Text = "";
        rbtnSex.SelectedValue = "";
      
        Dpmail.Text = "";
        txtmail.Text = "";
        txtAddress.Text = "";
        txtCity.Text = "";
      
        txtPinCode.Text = "";
        txtState.Text = "";
        txtLandlineNo.Text = "";
        txtMobileNo.Text = "";
        txtDD.Text = "";
        txtMM.Text = "";
        txtYY.Text = "";
        txtDDD.Text = "";
        txtMMM.Text = "";
        txtYYY.Text = "";
        txtReligon.Text = "";
        txtNationality.Text = "";
        txtDepartment.Text = "";
        Dprorg.SelectedValue = "";
        Dprtype.SelectedValue = "";
        txttotal.Text = "";
        txtposit.Text = "";
        txtS_BU.Text = "";
        txtS_OS.Text = "";
        txtS_Per.Text = "";
        txtS_Div.Text = "";
        txtSS_BU.Text = "";
        txtSS_OS.Text = "";
        txtSS_Div.Text = "";
        txtSS_Per.Text = "";
        txtWork.Text = "";
       
        txtMob1.Text = "";
        txtPlace1.Text = "";
        txtWork2.Text = "";
        txtPosition2.Text = "";
        txtMobile2.Text = "";
        txtPlace2.Text = "";
       txtRName1.Text = "";
        txtRWork1.Text = "";
        txtRMob1.Text = "";
        txtRPlace1.Text = "";
        txtRname2.Text = "";
        txtRWork2.Text = "";
        txtRMob2.Text = "";
        txtRPlace2.Text = "";
      
     
        
        }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (btnSubmit.Text == "Submit")
            {

                password = GeneralUtility.GetRandomPassword(6);

                Random rand = new Random();
                string extensions = string.Empty;
                string fileName = string.Empty;
                string pth = string.Empty;

                string strS_DDMMYY = txtS_DD.Text.Trim().Replace("'", "''") + "/" + txtS_MM.Text.Trim().Replace("'", "''") + "/" + txtS_YY.Text.Trim().Replace("'", "''");
                string strDDMMYY = txtDD.Text.Trim().Replace("'", "''") + "/" + txtMM.Text.Trim().Replace("'", "''") + "/" + txtYY.Text.Trim().Replace("'", "''");
                string strDDDMMMYYY = txtDDD.Text.Trim().Replace("'", "''") + "/" + txtMMM.Text.Trim().Replace("'", "''") + "/" + txtYYY.Text.Trim().Replace("'", "''");

                if (PhotoName.HasFile == true)
                {
                    //string strName = SqlHelp.GetSingleValue("Select RTRIM(LTRIM([PhotoName])) from Ragistration where RagistrationId='" + Session["ID"].ToString() + "'").ToString().Trim();
                    //System.IO.File.Delete(Server.MapPath(".") + "/photo//" + strName);
                    //System.IO.File.Delete(Server.MapPath(".") + "//photo/thumbnail/" + strName);
                    Stream strm = PhotoName.PostedFile.InputStream;
                    extensions = System.IO.Path.GetExtension(PhotoName.FileName.ToString().Trim());
                    fileName = rand.Next(1000, 10000).ToString().Trim() + "_" + txtName.Text.Trim().Replace("'", "''") + extensions;
                    pth = Server.MapPath(".") + "//resume//" + fileName.ToString().Trim();
                    //strThumbPath = Server.MapPath(".") + "//resume//" + fileName.ToString().Trim();
                    PhotoName.SaveAs(pth);


                }
                string strQry = "Declare @MasterId int" + System.Environment.NewLine;
                strQry += "INSERT INTO [Ragistration]" + System.Environment.NewLine;
                strQry += " ([Pref_Language],[Category],[Password],[RagistrationId]" + System.Environment.NewLine;
                strQry += ",[BankNo],[SalaryDate],[SalaryAmount],[BankName]" + System.Environment.NewLine;
                strQry += ",[Name],[FName],[Sex],[PhotoName]" + System.Environment.NewLine;
                strQry += ",[Dpmail],[mail],[Address],[City]" + System.Environment.NewLine;
                strQry += ",[PinCode],[State],[LandlineNo],[MobileNo]" + System.Environment.NewLine;
                strQry += ",[DOB],[DOJ],[Religion],[Nationality]" + System.Environment.NewLine;
                strQry += ",[Department],[org],[type],[total],[Posit]" + System.Environment.NewLine;
                strQry += ",[S_BU],[S_OS],[S_Per],[S_Div]" + System.Environment.NewLine;
                strQry += ",[SS_BU],[SS_OS],[SS_Per],[SS_Div]" + System.Environment.NewLine;
                strQry += ",[Work],[Position],[Mob1],[Place1],[Work2],[Position2],[Mobile2],[Place2]" + System.Environment.NewLine;
                strQry += ",[RName1],[RWork1],[RMob1],[RPlace1],[Rname2],[RWork2],[RMob2],[RPlace2])" + System.Environment.NewLine;
                strQry += "Values('" + rbtnPref_Language.SelectedValue + "','" + rbtnCategory.SelectedValue + "','" + txtRagistrationId.Text.Trim().Replace("'", "''") + "','"+ txtBankNo.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + strS_DDMMYY + "','" + txtSalaryAmount.Text.Trim().Replace("'", "''") + "','" + txtBankName.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtName.Text.Trim().Replace("'", "''") + "','" + txtFName.Text.Trim().Replace("'", "''") + "','" + rbtnSex.SelectedValue + "'," + System.Environment.NewLine;
                strQry += "'" + PhotoName.ToString() + "','" + Dpmail.SelectedValue + "','" + txtmail.Text.Trim().Replace("'", "''") + "','" + txtAddress.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtCity.Text.Trim().Replace("'", "''") + "','" + txtPinCode.Text.Trim().Replace("'", "''") + "','" + txtState.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtLandlineNo.Text.Trim().Replace("'", "''") + "','" + txtMobileNo.Text.Trim().Replace("'", "''") + "','" + strDDMMYY + "','" + strDDDMMMYYY + "'," + System.Environment.NewLine;
                strQry += "'" + txtReligon.Text.Trim().Replace("'", "''") + "','"+ txtNationality.Text.Trim().Replace("'", "''") + "','" + txtDepartment.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;

                strQry += "'" + Dprorg.SelectedValue + "','" + Dprtype.SelectedValue + "','" + txttotal.Text.Trim().Replace("'", "''") + "','" + txtposit.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtS_BU.Text.Trim().Replace("'", "''") + "','" + txtS_OS.Text.Trim().Replace("'", "''") + "','" + txtS_Per.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtS_Div.Text.Trim().Replace("'", "''") + "','" + txtSS_BU.Text.Trim().Replace("'", "''") + "','" + txtSS_OS.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtSS_Div.Text.Trim().Replace("'", "''") + "','" + txtSS_Per.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtWork.Text.Trim().Replace("'", "'") + "','" + txtPosition.Text.Trim().Replace("'", "'") + "'," + System.Environment.NewLine;
                strQry += "'" +  txtMob1.Text.Trim().Replace("'", "''") + "','" + txtPlace1.Text.Trim().Replace("'", "''") + "','" + txtWork2.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtPosition2.Text.Trim().Replace("'", "''") + "','" + txtMobile2.Text.Trim().Replace("'", "''") + "','" + txtPlace2.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtRName1.Text.Trim().Replace("'", "''") + "','" + txtRWork1.Text.Trim().Replace("'", "''") + "','" + txtRMob1.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtRPlace1.Text.Trim().Replace("'", "'") + "','" + txtRname2.Text.Trim().Replace("'", "''") + "','" + txtRWork2.Text.Trim().Replace("'", "''") + "'," + System.Environment.NewLine;
                strQry += "'" + txtRMob2.Text.Trim().Replace("'", "''") + "','" + txtRPlace2.Text.Trim().Replace("'", "''") + "')" + System.Environment.NewLine;
                strQry += " set @MasterId=@@IDENTITY" + System.Environment.NewLine;
                SqlHelp.ForExecuteNonQuery(strQry);
                strQry = "SELECT @MasterId as MaxId";
                DataTable dtID = SqlHelp.GetDataTable(strQry);
                if (dtID.Rows.Count > 0)
                {
                    Session["ID"] = dtID.Rows[0]["MaxId"].ToString();
                    Email.EmailToAdmin("Ragistration", Mail_Admin());
                    //Email.EmailToUser(Dpmail.Text, Mail_User(), "Online Ragistration : Hindi & English");

                    Clear();
                    Response.Redirect("viewuser.aspx");
                }
            }
            else if (btnSubmit.Text == "Update")
            {
                string strUpdate = string.Empty;
                Random rand = new Random();
                string extensions = string.Empty;
                string fileName = string.Empty;
                string pth = string.Empty;
                // string strThumbPath = string.Empty;
                string strS_DDMMYY = txtS_DD.Text.Trim().Replace("'", "''") + "/" + txtS_MM.Text.Trim().Replace("'", "''") + "/" + txtS_YY.Text.Trim().Replace("'", "''");
                string strDDMMYY = txtDD.Text.Trim().Replace("'", "''") + "/" + txtMM.Text.Trim().Replace("'", "''") + "/" + txtYY.Text.Trim().Replace("'", "''");
                string strDDDMMMYYY = txtDDD.Text.Trim().Replace("'", "''") + "/" + txtMMM.Text.Trim().Replace("'", "''") + "/" + txtYYY.Text.Trim().Replace("'", "''");

                if (PhotoName.HasFile == true)
                {
                    string strName = SqlHelp.GetSingleValue("Select RTRIM(LTRIM([PhotoName])) from Ragistration where RagistrationId='" + Session["ID"].ToString() + "'").ToString().Trim();
                    System.IO.File.Delete(Server.MapPath(".") + "/resume//" + strName);
                    System.IO.File.Delete(Server.MapPath(".") + "//resume/" + strName);
                    Stream strm = PhotoName.PostedFile.InputStream;
                    extensions = System.IO.Path.GetExtension(PhotoName.FileName.ToString().Trim());
                    fileName = rand.Next(1000, 10000).ToString().Trim() + "_" + txtName.Text.Trim().Replace("'", "''") + extensions;
                    pth = Server.MapPath(".") + "//resume//" + fileName.ToString().Trim();
                    //strThumbPath = Server.MapPath(".") + "//resume//" + fileName.ToString().Trim();
                    PhotoName.SaveAs(pth);


                }
                strUpdate += "UPDATE [Ragistration]" + System.Environment.NewLine;
                strUpdate += "SET[Pref_Language] = '" + rbtnPref_Language.SelectedValue + "'" + System.Environment.NewLine;
                strUpdate += ", [Category] = '" + rbtnCategory.SelectedValue + "'" + System.Environment.NewLine;
                strUpdate += ",[Password] = '" + txtPassword.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[RagistrationId] = '" + txtRagistrationId.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[BankNo] = '" + txtBankNo.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[S_DDMMYY] = '" + strS_DDMMYY + "'" + System.Environment.NewLine;
                strUpdate += ",[SalaryAmount] = '" + txtSalaryAmount.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[BankName] = '" + txtBankName.ToString() + "'" + System.Environment.NewLine;
                strUpdate += ",[Name] = '" + txtName.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;

                strUpdate += " ,[FName] = '" + txtFName.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += " ,[Sex] = '" + rbtnSex.SelectedValue + "'" + System.Environment.NewLine;
                strUpdate += " ,[PhotoName] = '" + PhotoName.ToString() + "'" + System.Environment.NewLine;
                strUpdate += ",[mail] = '" + txtmail.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Dpmail] = '" + Dpmail.SelectedValue + "'" + System.Environment.NewLine;
               
                strUpdate += " ,[Address] = '" + txtAddress.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += " ,[City] = '" + txtCity.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += " ,[PinCode] = '" + txtPinCode.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += " ,[Posit] = '" + txtposit.Text.Trim().Replace("'", "'") + "'" + System.Environment.NewLine;
                strUpdate += " ,[State] = '" + txtState.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += " ,[LandlineNo] = '" + txtLandlineNo.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += " ,[MobileNo] = '" + txtMobileNo.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += " ,[DDMMYY] = '" + strDDMMYY + "'" + System.Environment.NewLine;
                strUpdate += " ,[DDDMMMYYY] = '" + strDDDMMMYYY + "'" + System.Environment.NewLine;
                strUpdate += ",[Religon] = '" + txtReligon.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += " ,[Nationality] = '" + txtNationality.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;

                strUpdate += ",[Department] = '" + txtDepartment.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[org] = '" + Dprorg.SelectedValue + "'" + System.Environment.NewLine;
                strUpdate += ",[type] = '" + Dprtype.SelectedValue + "'" + System.Environment.NewLine;
                strUpdate += ",[total] = '" + txttotal.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Posit] = '" + txtposit.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[S_BU] = '" + txtS_BU.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[S_OS] = '" + txtS_OS.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                // strUpdate += ",[S_MM] = '" + txtS_MM.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                // strUpdate += ",[S_MO] = '" + txtS_MO.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[S_Per] = '" + txtS_Per.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[S_Div] = '" + txtS_Div.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[SS_BU] = '" + txtSS_BU.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[SS_OS] = '" + txtSS_OS.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                // strUpdate += ",[SS_MM] = '" + txtSS_MM.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                // strUpdate += ",[SS_MO] = '" + txtSS_MO.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[SS_Per] = '" + txtSS_Per.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[SS_Div] = '" + txtSS_Div.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Work] = '" + txtWork.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Position] = '" + txtPosition.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Mob1] = '" + txtMob1.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Place1] = '" + txtPlace1.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Work2] = '" + txtWork2.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Position2] = '" + txtPosition2.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Mobile2] = '" + txtMobile2.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Place2] = '" + txtPlace2.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
              
                strUpdate += ",[RName1] = '" + txtRName1.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[RWork1] = '" + txtRWork1.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[RMob1] = '" + txtRMob1.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[RPlace1] = '" + txtRPlace1.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[Rname2] = '" + txtRname2.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[RWork2] = '" + txtRWork2.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[RMob2] = '" + txtRMob2.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
                strUpdate += ",[RPlace2] = '" + txtRPlace2.Text.Trim().Replace("'", "''") + "'" + System.Environment.NewLine;
               
                
                strUpdate += "Where RagistrationId='" + Session["ID"].ToString() + "'" + System.Environment.NewLine;
                if (SqlHelp.ForExecuteNonQuery(strUpdate)==true)
                {
                    btnSubmit.Text = "Submit";
                    lblmsg.Text = "All detail succefully";
                    Response.Redirect("viewuser.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            throw (ex);


        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Clear();
    }

    protected void FillUpdate()
    {
        try
        {

            DataTable dtUpdate = SqlHelp.GetDataTable("Select * from Ragistration Where RagistrationId='" + Session["RagistrationId"].ToString() + "'");
            if (dtUpdate.Rows.Count > 0)
            {
                rbtnPref_Language.SelectedValue = dtUpdate.Rows[0]["Pref_Language"].ToString();
                rbtnCategory.SelectedValue = dtUpdate.Rows[0]["Category"].ToString();
                txtPassword.Text = dtUpdate.Rows[0]["Password"].ToString();
                txtRagistrationId.Text = dtUpdate.Rows[0]["RagistrationId"].ToString();
               
                txtBankNo.Text = dtUpdate.Rows[0]["BankNo"].ToString();


                DateTime sadate = Convert.ToDateTime(dtUpdate.Rows[0]["S_DDMMYY"].ToString());
                txtS_DD.Text = sadate.Day.ToString();
                txtS_MM.Text = sadate.Month.ToString();
                txtS_YY.Text = sadate.Year.ToString();


                txtSalaryAmount.Text = dtUpdate.Rows[0]["SalaryAmount"].ToString();


                txtBankName.Text = dtUpdate.Rows[0]["BankName"].ToString();

                txtName.Text = dtUpdate.Rows[0]["Name"].ToString();
                txtFName.Text = dtUpdate.Rows[0]["FName"].ToString();
                rbtnSex.SelectedValue = dtUpdate.Rows[0]["Sex"].ToString();
               // PhotoName.Text = dtUpdate.Rows[0]["PhotoName"].ToString();
                txtAddress.Text = dtUpdate.Rows[0]["Address"].ToString();
                txtCity.Text = dtUpdate.Rows[0]["City"].ToString();
                txtPinCode.Text = dtUpdate.Rows[0]["PinCode"].ToString();
                txtState.Text = dtUpdate.Rows[0]["State"].ToString();
                txtLandlineNo.Text = dtUpdate.Rows[0]["LandlineNo"].ToString();
                txtMobileNo.Text = dtUpdate.Rows[0]["MobileNo"].ToString();

                DateTime dbdate = Convert.ToDateTime(dtUpdate.Rows[0]["DDMMYY"].ToString());
                txtDD.Text = dbdate.Day.ToString();
                txtMM.Text = dbdate.Month.ToString();
                txtYY.Text = dbdate.Year.ToString();

                DateTime dojdate = Convert.ToDateTime(dtUpdate.Rows[0]["DDDMMMYYY"].ToString());
                txtDDD.Text = dojdate.Day.ToString();
                txtMMM.Text = dojdate.Month.ToString();
                txtYYY.Text = dojdate.Year.ToString();

                
                txtReligon.Text = dtUpdate.Rows[0]["Religion"].ToString();
                txtNationality.Text = dtUpdate.Rows[0]["Nationality"].ToString();
                txtDepartment.Text = dtUpdate.Rows[0]["Department"].ToString();
                Dprorg.SelectedValue = dtUpdate.Rows[0]["org"].ToString();
                Dprtype.SelectedValue = dtUpdate.Rows[0]["type"].ToString();
                txttotal.Text = dtUpdate.Rows[0]["total"].ToString();
                
                txtposit.Text = dtUpdate.Rows[0]["Posit"].ToString();

                txtS_BU.Text = dtUpdate.Rows[0]["S_BU"].ToString();
                txtS_OS.Text = dtUpdate.Rows[0]["S_OS"].ToString();
                //  txtS_MM.Text = dtUpdate.Rows[0]["S_MM"].ToString();
                // txtS_MO.Text = dtUpdate.Rows[0]["S_MO"].ToString();
                txtS_Per.Text = dtUpdate.Rows[0]["S_Per"].ToString();
                txtS_Div.Text = dtUpdate.Rows[0]["S_Div"].ToString();

                txtSS_BU.Text = dtUpdate.Rows[0]["SS_BU"].ToString();
                txtSS_OS.Text = dtUpdate.Rows[0]["SS_OS"].ToString();
                // txtSS_MM.Text = dtUpdate.Rows[0]["SS_MM"].ToString();
                //txtSS_MO.Text = dtUpdate.Rows[0]["SS_MO"].ToString();
                txtSS_Per.Text = dtUpdate.Rows[0]["SS_Per"].ToString();
                txtSS_Div.Text = dtUpdate.Rows[0]["SS_Div"].ToString();
                txtWork.Text = dtUpdate.Rows[0]["Work"].ToString();
                txtPosition.Text = dtUpdate.Rows[0]["Position"].ToString();
                txtMob1.Text = dtUpdate.Rows[0]["Mob1"].ToString();
                txtPlace1.Text = dtUpdate.Rows[0]["Place1"].ToString();
                txtWork2.Text = dtUpdate.Rows[0]["Work2"].ToString();
                txtPosition2.Text = dtUpdate.Rows[0]["Position2"].ToString();
                txtMobile2.Text = dtUpdate.Rows[0]["Mobile2"].ToString();
                txtPlace2.Text = dtUpdate.Rows[0]["Place2"].ToString(); 
                txtRName1.Text = dtUpdate.Rows[0]["RName1"].ToString();
                txtRWork1.Text = dtUpdate.Rows[0]["RWork1"].ToString();
                txtRMob1.Text = dtUpdate.Rows[0]["RMob1"].ToString();
                txtRPlace1.Text = dtUpdate.Rows[0]["RPlace1"].ToString();
                txtRname2.Text = dtUpdate.Rows[0]["RName2"].ToString();
                txtRWork2.Text = dtUpdate.Rows[0]["RWork2"].ToString();
                txtRMob2.Text = dtUpdate.Rows[0]["RMob2"].ToString();
                txtRPlace2.Text = dtUpdate.Rows[0]["RPlace2"].ToString();

               


                btnSubmit.Text = "Update";
            }
        }
         catch (Exception ex)
        {
            throw (ex);

        }
        }
    

    /// <summary>
    /// Body css for Email to User
    /// </summary>
    /// <returns></returns>
//    protected string Mail_Admin()
//    {
//        try
//        {
//            string strBody = string.Empty;
//            strBody = @"<html><head></head>
//                        <style type='text/css'>
//                            .ibdr {
//	                                border: 1px dashed #999999;
//	                                background-color: #E9E9E9;
//                                }
//                                                            .toprk {
//                                font-family: Calibri;
//                                font-size: 28px;
//                                line-height: 30px;
//                                color: #FF8B00;
//                                text-decoration: none;
//                                font-weight: bold;
//                                }
//                                                            .title03 {
//	                                font-family: Calibri;
//	                                font-size: 20px;
//	                                line-height: 30px;
//	                                font-weight: bold;
//	                                color: #666666;
//	                                padding-bottom: 3px;
//	                                }
//                                                           .btn_01 {
//	                                font-family: Calibri;
//	                                font-size: 16px;
//	                                font-weight: normal;
//	                                color: #333333;
//	                                text-decoration: none;
//	                                line-height: 22px;
//	                                border-bottom-style: none;
//	                                text-transform: capitalize;
//                                }
//                                                            .btn_06 {
//	                                font-family: Calibri;
//	                                font-size: 15px;
//	                                font-weight: bold;
//	                                color: #333333;
//	                                text-decoration: none;
//	                                line-height: 25px;
//                                }
//                                .btn_02 {
//	                                font-family: Arial, Helvetica, sans-serif, Tahoma;
//	                                font-size: 13px;
//	                                font-weight: bold;
//	                                color: #FE8A00;
//	                                text-decoration: none;
//	                                line-height: 23px;
//	                                padding-bottom: 3px;
//                                }
//                            
//                            </style>
//                            <body>" + ThanksMessage() + @"</body></html>";
//            return strBody;


    //    }
    //    catch (Exception ex)
    //    {
    //        throw (ex);
    //    }

    //}



    //Mail Message For User
    //protected string ThanksMessage()
    //{
    //    string strMsg = string.Empty;
    //    strMsg += "<table width='98%' border='0' align='center' cellpadding='0' cellspacing='0' class='ibdr'>" + System.Environment.NewLine;
    //    strMsg += " <tr><td>" + System.Environment.NewLine;
    //    strMsg += "<table width='98%' border='0' align='center' cellpadding='0' cellspacing='4'>" + System.Environment.NewLine;
    //    strMsg += "<tr><td align='center'>&nbsp;</td></tr>" + System.Environment.NewLine;
    //    strMsg += " <tr><td height='28' align='center' class='toprk'>" + System.Environment.NewLine;
    //    strMsg += "<span class='topHeading'>Dear " + txtName.Text.Trim().Replace("'", "''") + ",</span>" + System.Environment.NewLine;
    //    strMsg += "</td></tr>" + System.Environment.NewLine;
    //    strMsg += "<tr><td height='28' align='center' class='title03'>" + System.Environment.NewLine;
    //    strMsg += "Congratulations ! You have been successfully registered ." + System.Environment.NewLine;

    //    strMsg += "</td></tr>" + System.Environment.NewLine;
    //    strMsg += "<tr><td height='28' align='center' class='btn_01'>" + System.Environment.NewLine;
    //    strMsg += "<strong>Please find login details as below :</strong>" + System.Environment.NewLine;
    //    strMsg += "</td></tr>" + System.Environment.NewLine;
    //    strMsg += "<tr><td height='28' align='center' class='btn_06'>" + System.Environment.NewLine;
    //    strMsg += "Email Id :&nbsp;" + Dpmail.Text.Trim() + "" + System.Environment.NewLine;
    //    strMsg += "</td></tr>" + System.Environment.NewLine;
    //    strMsg += "<tr><td height='28' align='center' class='btn_06'>" + System.Environment.NewLine;
    //    strMsg += "Password:&nbsp;" + password + "" + System.Environment.NewLine;
    //    strMsg += "</td></tr>" + System.Environment.NewLine;
    //    strMsg += "<tr><td height='28' align='center' class='btn_02'>" + System.Environment.NewLine;
    //    strMsg += "For more information, please email us at<a href='mailto:dsr@agritech.com'> dsr@agritech.com</a>" + System.Environment.NewLine;
    //    strMsg += "</td></tr>" + System.Environment.NewLine;
    //    //Footer Part
    //    strMsg += "<tr><td height='42' align='center' class='btn_01'>" + System.Environment.NewLine;
    //    strMsg += "<strong>Thanks &amp; Regards,<br />Co-ordinator(Hindi & English)</strong>" + System.Environment.NewLine;
    //    strMsg += "</td></tr>" + System.Environment.NewLine;
    //    strMsg += "<tr><td align='center'>&nbsp;</td></tr>" + System.Environment.NewLine;
    //    strMsg += "</table>" + System.Environment.NewLine;
    //    strMsg += "</td></tr>" + System.Environment.NewLine;
    //    strMsg += "</table>" + System.Environment.NewLine;
    //    return strMsg;
    //}

    // <summary>
    // Email Function for Admin
    // </summary>
   //  <returns></returns>
    protected string Mail_Admin()
    {
        try
        {
            string strMsg = string.Empty;
            strMsg += "<table cellpadding='0' cellspacing='0' width='500'>" + System.Environment.NewLine;
            strMsg += "<tr><td colspan='2'>&nbsp;</td> </tr>" + System.Environment.NewLine;
            strMsg += "<tr><td colspan='2' style='font-size: medium; font-weight: 700; font-family: Arial, Helvetica, sans-serif; color: #000000'>" + System.Environment.NewLine;
            strMsg += "&nbsp;&nbsp;&nbsp;Online Member Ragistration</td></tr>" + System.Environment.NewLine;
            strMsg += " <tr><td colspan='2'>&nbsp; </td></tr>" + System.Environment.NewLine;
            strMsg += "<tr><td colspan='2'>" + System.Environment.NewLine;
            strMsg += "<table width='500' border='0' align='center' cellpadding='4' cellspacing='1' bgcolor='#EEEEEE'style='font-family: Verdana; font-size: 12px'>" + System.Environment.NewLine;
            strMsg += "<tr><td height='28' align='center' class='btn_06'>" + System.Environment.NewLine;
                strMsg += "Password:&nbsp;" + password + "" + System.Environment.NewLine;
            if (txtName.Text != "")
            {
                strMsg += " <tr><td >&nbsp;&nbsp;&nbsp;UserName :</td><td>" + txtName.Text + "</td></tr>" + System.Environment.NewLine;
            }
           
            if (txtmail.Text != "")
            {
                strMsg += " <tr><td >&nbsp;&nbsp;&nbsp;mail :</td><td>" + txtmail.Text + "</td></tr>" + System.Environment.NewLine;
            }
            if (txtLandlineNo.Text != "")
            {
                strMsg += " <tr><td >&nbsp;&nbsp;&nbsp;Landline :</td><td>" + txtLandlineNo.Text + "</td></tr>" + System.Environment.NewLine;
            }
            if (txtMobileNo.Text != "")
            {
                strMsg += " <tr><td >&nbsp;&nbsp;&nbsp;Mobile No. :</td><td>" + txtMobileNo.Text + "</td></tr>" + System.Environment.NewLine;
            }
            if (txtAddress.Text != "")
            {
                strMsg += " <tr><td >&nbsp;&nbsp;&nbsp;MalingAddress :</td><td>" + txtAddress.Text + "</td></tr>" + System.Environment.NewLine;
            }
            if (txtCity.Text != "")
            {
                strMsg += " <tr><td >&nbsp;&nbsp;&nbsp;City :</td><td>" + txtCity.Text + "</td></tr>" + System.Environment.NewLine;
            }
           
            strMsg += "</table> </td></tr></table>";
            return strMsg;
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }


    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/Home.aspx");
    }

    }
