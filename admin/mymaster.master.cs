﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_mymaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    protected void imgLogout_Click(object sender, ImageClickEventArgs e)
    {
        Session["AdminID"] = null;
        Response.Redirect("index.aspx",false);
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Session["AdminID"] = null;
        Response.Redirect("index.aspx", false);
    }
}
