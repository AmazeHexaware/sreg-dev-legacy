﻿<%@ Page Language="C#" MasterPageFile="~/admin/mymaster.master" AutoEventWireup="true" CodeFile="Ragistrations.aspx.cs" Inherits="admin_Ragistrations" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        function checkFileExtension(elem, obj) {
            var browserName = navigator.appName;
            var filePath = elem.value;

            if (filePath.indexOf('.') == -1)
                return false;

            var validExtensions = new Array();
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

            validExtensions[0] = '.txt';
            validExtensions[1] = '.html';
            validExtensions[2] = '.pdf';            
            validExtensions[3] = 'docx';
            validExtensions[4] = 'doc';
             for (var i = 0; i < validExtensions.length; i++) {
                if (ext == validExtensions[i]) {

                    if (elem.files[0].size <full ) {
                        return true;
                    }
                    else {
                        elem.value = "";
                        alert("Please Check Upload File Size.")
                        return false;
                    }
                }
           
              }
            
            alert('The file extension ' + ext.toUpperCase() + ' is not allowed!');
            if (browserName == "Microsoft Internet Explorer") {
                document.getElementById(obj).innerHTML = document.getElementById(obj).innerHTML;

            }
            else if (browserName == "Netscape") {

                elem.value = "";
                elem.click();
            }
            return false;
        }

        function CheckFieldValidation() {
        
            // salary amount validation           
            if (trim(document.getElementById('<%=txtSalaryAmount.ClientID %>').value).length == 0) {
                alert("Salary Amount Required");
                document.getElementById('<%=txtSalaryAmount.ClientID %>').focus();
                return false;
            }
             //Optional Subjects Validation
            if (trim(document.getElementById('<%=txtPassword.ClientID %>').value).length == 0) {
                alert("Password Required");
                document.getElementById('<%=txtPassword.ClientID %>').focus();
                return false;
            }
             //Optional Subjects Validation
            if (trim(document.getElementById('<%=txtRagistrationId.ClientID %>').value).length == 0) {
                alert("id Required");
                document.getElementById('<%=txtRagistrationId.ClientID %>').focus();
                return false;
            }
           
           
            // Salary date validation           
            if (trim(document.getElementById('<%=txtS_DD.ClientID %>').value).length == 0) {
                alert("Salary Date  Required");
                document.getElementById('<%=txtS_DD.ClientID %>').focus();
                return false;
            }
              //Salary month validation           
            if (trim(document.getElementById('<%=txtS_MM.ClientID %>').value).length == 0) {
                alert("Salary Month Required");
                document.getElementById('<%=txtS_MM.ClientID %>').focus();
                return false;
            }
            // Salary validation           
            if (trim(document.getElementById('<%=txtS_YY.ClientID %>').value).length == 0) {
                alert("Salary Required");
                document.getElementById('<%=txtS_YY.ClientID %>').focus();
                return false;
            }
            //Name Validation
            if (trim(document.getElementById('<%=txtName.ClientID %>').value).length == 0) {
                alert("Name Required");
                document.getElementById('<%=txtName.ClientID %>').focus();
                return false;
            }
            //Father Name Validation
            if (trim(document.getElementById('<%=txtFName.ClientID %>').value).length == 0) {
                alert("Father's Name Required");
                document.getElementById('<%=txtFName.ClientID %>').focus();
                return false;
            }
//            //Photo Validation
//            if (document.getElementById('<%=PhotoName.ClientID %>').value == "") {
//                alert("Resume Required");
//                document.getElementById('<%=PhotoName.ClientID %>').click();
//                return false;
//            }

            //Email Validation
            if (trim(document.getElementById('<%=Dpmail.ClientID %>').value).length == 0) {
                alert("Email Id Required");
                document.getElementById('<%=Dpmail.ClientID %>').focus();
                return false;
            }
            //Email Employee Validation
               if (trim(document.getElementById('<%=txtmail.ClientID %>').value).length == 0) {
                alert("Employee mail  Required");
                document.getElementById('<%=txtmail.ClientID %>').focus();
                return false;
            }
           //Address Validation
            if (trim(document.getElementById('<%=txtAddress.ClientID %>').value).length == 0) {
                alert("Address Required");
                document.getElementById('<%=txtAddress.ClientID %>').focus();
                return false;
                }
          //City Validation
            if (trim(document.getElementById('<%=txtCity.ClientID %>').value).length == 0) {
                alert("City Required");
                document.getElementById('<%=txtCity.ClientID %>').focus();
                return false;
            }
            //ZipCode Validation
            if (trim(document.getElementById('<%=txtPinCode.ClientID %>').value).length == 0) {
                alert("Pin Code Required");
                document.getElementById('<%=txtPinCode.ClientID %>').focus();
                return false;
            }
           //State Validation
            if (trim(document.getElementById('<%=txtState.ClientID %>').value).length == 0) {
                alert("State Required");
                document.getElementById('<%=txtState.ClientID %>').focus();
                return false;}
             //Mobile Validation
            if (trim(document.getElementById('<%=txtMobileNo.ClientID %>').value).length == 0) {
                alert("Mobile Required");
                document.getElementById('<%=txtMobileNo.ClientID %>').focus();
                return false;
            }
            //Date Validation
            if (trim(document.getElementById('<%=txtDD.ClientID %>').value).length == 0) {
                alert("Date Required");
                document.getElementById('<%=txtDD.ClientID %>').focus();
                return false;
            }
            //Month Validation
            if (trim(document.getElementById('<%=txtMM.ClientID %>').value).length == 0) {
            
                alert("Month Required");
                document.getElementById('<%=txtMM.ClientID %>').focus();
                return false;
            }
                //Year Validation
            if (trim(document.getElementById('<%=txtYY.ClientID %>').value).length == 0) {
                alert("Year Required");
                document.getElementById('<%=txtYY.ClientID %>').focus();
                return false;
            }
            //Date Validation
            if (trim(document.getElementById('<%=txtDDD.ClientID %>').value).length == 0) {
                alert("Date Required");
                document.getElementById('<%=txtDDD.ClientID %>').focus();
                return false;
            }
            //Month Validation
            if (trim(document.getElementById('<%=txtMMM.ClientID %>').value).length == 0) {
            
                alert("Month Required");
                document.getElementById('<%=txtMMM.ClientID %>').focus();
                return false;
            }
             //Year Validation
            if (trim(document.getElementById('<%=txtYYY.ClientID %>').value).length == 0) {
                alert("Year Required");
                document.getElementById('<%=txtYYY.ClientID %>').focus();
                return false;
            }
              //Religion Validation
            if (trim(document.getElementById('<%=txtReligon.ClientID %>').value).length == 0) {
                alert("Religion Required");
                document.getElementById('<%=txtReligon.ClientID %>').focus();
                return false;
            }
             //Nationality Validation
            if (trim(document.getElementById('<%=txtNationality.ClientID %>').value).length == 0) {
                alert("Nationality Required");
                document.getElementById('<%=txtNationality.ClientID %>').focus();
                return false;
            }
              //Department Validation
            if (trim(document.getElementById('<%=txtDepartment.ClientID %>').value).length == 0) {
                alert("Department Name Required");
                document.getElementById('<%=txtDepartment.ClientID %>').focus();
                return false;
            }
            //Total Leave Vallidation
            if (trim(document.getElementById('<%=txttotal.ClientID %>').value).length == 0) {
                alert("Total Leave Required");
                document.getElementById('<%=txttotal.ClientID %>').focus();
                return false;
            }
              //Position Subjects Validation
            if (trim(document.getElementById('<%=txtposit.ClientID %>').value).length == 0) {
                alert("Company Position Required");
                document.getElementById('<%=txtposit.ClientID %>').focus();
                return false;
            }
          //Board/University Validation
            if (trim(document.getElementById('<%=txtS_BU.ClientID %>').value).length == 0) {
                alert("Board/University Required");
                document.getElementById('<%=txtS_BU.ClientID %>').focus();
                return false;
            }


            //Optional Subjects Validation
            if (trim(document.getElementById('<%=txtS_OS.ClientID %>').value).length == 0) {
                alert("Optional Subjects Required");
                document.getElementById('<%=txtS_OS.ClientID %>').focus();
                return false;
            }
            //Percentage Validation
            if (trim(document.getElementById('<%=txtS_Per.ClientID %>').value).length == 0) {
                alert("Percentage Required");
                document.getElementById('<%=txtS_Per.ClientID %>').focus();
                return false;
            }
            //Division Validation
            if (trim(document.getElementById('<%=txtS_Div.ClientID %>').value).length == 0) {
                alert("Division Required");
                document.getElementById('<%=txtS_Div.ClientID %>').focus();
                return false;
            }
            //Board/University Validation
////            if (trim(document.getElementById('<%=txtSS_BU.ClientID %>').value).length == 0) {
////                alert("Board/University Required");
////                document.getElementById('<%=txtSS_BU.ClientID %>').focus();
////                return false;
////            }
////            //Optional Subjects Validation
////            if (trim(document.getElementById('<%=txtSS_OS.ClientID %>').value).length == 0) {
////                alert("Optional Subjects Required");
////                document.getElementById('<%=txtSS_OS.ClientID %>').focus();
////                return false;
////            }
            //Percentage Validation
////            if (trim(document.getElementById('<%=txtSS_Per.ClientID %>').value).length == 0) {
////                alert("Percentage Required");
////                document.getElementById('<%=txtSS_Per.ClientID %>').focus();
////                return false;
////            }
            //Division Validation
////            if (trim(document.getElementById('<%=txtSS_Div.ClientID %>').value).length == 0) {
////                alert("Division Required");
////                document.getElementById('<%=txtSS_Div.ClientID %>').focus();
////                return false;
////            }
          //CheckBox Agree
            if (document.getElementById('<%=chkAgree.ClientID %>') != null) {
                //Checkbox Validation
                if (document.getElementById('<%=chkAgree.ClientID %>').checked == false && document.getElementById('<%=chkAgree.ClientID %>') != null) {
                    alert("Please Accept Terms & Condition");
                    document.getElementById('<%=chkAgree.ClientID %>').focus();
                    return false;
                }
            }

            //return true;



        }
        function trim(str) {
            return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
        }
        function validateEmail() {
            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if (emailPattern.test(trim(document.getElementById('<%=Dpmail.ClientID %>').value)) == false) {
                alert("Select  Email Id");
                document.getElementById('<%=Dpmail.ClientID %>').value = "";
                document.getElementById('<%=Dpmail.ClientID %>').focus();
                return false;
            }
            return true;
        }   
    </script>

    <table id="table1" width="800px" align="center" 
        style="background-image: url('../images/bg_top.jpg')">
          <%-- <tr>
            <td>
                Roll No.
            </td>
            <td>
                <asp:TextBox ID="txtRollNo" runat="server"></asp:TextBox>
            </td>
        </tr>--%>
        <tr>
            <td align="center">
                <asp:Label ID="lblmsg" runat="server" height="25px"></asp:Label>
            </td>            
        </tr>
        <tr>
          <td class="txt">
                <fieldset>
                    <legend>Language Type </legend>
                    <table width="100%">
                        <tr>
                            <td width="40%">
                                Preference for Language</td>
                            <td class="txt">
                                <asp:RadioButtonList ID="rbtnPref_Language" runat="server" RepeatColumns="6" 
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem>Hindi</asp:ListItem>
                                    <asp:ListItem Selected="True">English</asp:ListItem>
                                </asp:RadioButtonList>
                            &nbsp;</td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td class="txt">
                <fieldset>
                    <legend>Category Type </legend>
                    <table width="100%">
                        <tr>
                            <td width="40%">
                                Category:
                            </td>
                            <td class="txt">
                                <asp:RadioButtonList ID="rbtnCategory" runat="server" RepeatColumns="6" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Gen</asp:ListItem>
                                    <asp:ListItem>SC</asp:ListItem>
                                    <asp:ListItem>ST</asp:ListItem>
                                    <asp:ListItem>OBC</asp:ListItem>
                                    <asp:ListItem>PH</asp:ListItem>
                                    <asp:ListItem>DB</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend>Bank Details </legend>
                    <table width="100%">
                        <tr>
                            <td class="txt" style="width: 20%">
                                Password</td>
                            <td style="width: 150px">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="txtPassword" ErrorMessage="Enter correct password" 
                                    ToolTip="Enter correct password" Visible="False"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtPassword" runat="server" Height="16px" TextMode="Password" 
                                    Width="193px"></asp:TextBox>
                            </td>
                            <td class="txt">
                                R.Id&nbsp;
                            </td>
                            <td>
                                &nbsp;
                                <asp:TextBox ID="txtRagistrationId" onchange="filldata();" 
                                    CssClass="formtext_011" runat="server"
                                    MaxLength="6"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtRagistrationId_FilteredTextBoxExtender" runat="server"
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtRagistrationId">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 20%">
                                Bank A/c No</td>
                            <td style="width: 150px">
                                <asp:TextBox ID="txtBankNo" onchange="filldata();" CssClass="formtext_011" runat="server"
                                    MaxLength="6"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtBankNo_FilteredTextBoxExtender" runat="server"
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtBankNo">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="txt">
                                Salary Date
                            </td>
                            <td>
                                <span class="redtext">DD</span><asp:TextBox ID="txtS_DD" runat="server" Width="35px"
                                    MaxLength="2"></asp:TextBox><cc1:FilteredTextBoxExtender ID="txtS_DD_FilteredTextBoxExtender"
                                        runat="server" Enabled="True" FilterType="Numbers" 
                                    TargetControlID="txtS_DD">
                                    </cc1:FilteredTextBoxExtender>
                                <span class="redtext">MM</span><asp:TextBox ID="txtS_MM" runat="server" Width="35px"
                                    MaxLength="2"></asp:TextBox><cc1:FilteredTextBoxExtender ID="txtS_MM_FilteredTextBoxExtender"
                                        runat="server" Enabled="True" FilterType="Numbers" 
                                    TargetControlID="txtS_MM">
                                    </cc1:FilteredTextBoxExtender>
                                <span class="redtext">YY</span><asp:TextBox ID="txtS_YY" runat="server" Width="55px"
                                    onchange="filldata();" MaxLength="4"></asp:TextBox><cc1:FilteredTextBoxExtender ID="txtS_YY_FilteredTextBoxExtender"
                                        runat="server" Enabled="True" FilterType="Numbers" 
                                    TargetControlID="txtS_YY">
                                    </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 20%">
                                Salary</td>
                            <td style="width: 150px">
                                <asp:TextBox ID="txtSalaryAmount" onchange="filldata();" CssClass="formtext_011" runat="server"
                                    MaxLength="6"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtSalaryAmount_FilteredTextBoxExtender" runat="server"
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtSalaryAmount">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td class="txt">
                                Bank Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtBankName" onchange="filldata();" CssClass="formtext_011" runat="server"
                                    MaxLength="150"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="height: 560px">
                <fieldset>
                    <legend>Personal Information </legend>
                    <table width="100%">
                        <tr>
                            <td class="txt" style="width: 42%">
                                Name Of Employee
                            </td>
                            <td>
                                <asp:TextBox ID="txtName" CssClass="formtext_012" runat="server" MaxLength="100"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Father&#39;s Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtFName" CssClass="formtext_012" runat="server" MaxLength="100"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Gender
                            </td>
                            <td class="txt">
                                <asp:RadioButtonList ID="rbtnSex" runat="server" RepeatColumns="6" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True">Male</asp:ListItem>
                                    <asp:ListItem>Female</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Upload Resume
                            </td>
                            <td>
                                <div id="uploadFile_div">
                                    <asp:FileUpload ID="PhotoName" runat="server" onchange="return checkFileExtension(this,'uploadFile_div');"
                                        ToolTip="Uploaden profiel foto (<em>.txt,.doc,.docx)" />
                                </div>
                            </td>
                        </tr>
                        <%--<tr>
                            <td class="txt">
                                Upload Signature
                            </td>
                            <td>
                                <asp:FileUpload ID="fpSignName" runat="server" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Email ID
                                (Head Mail)</td>
                            <td>
                                <asp:DropDownList ID="Dpmail" runat="server" Height="19px" Width="195px">
                                    <asp:ListItem>dsr@agritech.com</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Email ID(Employee ID)</td>
                            <td>
                                <asp:TextBox ID="txtmail" onchange="return validateEmail();" CssClass="formtext_email"
                                    runat="server" MaxLength="100"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Mailing Address
                            </td>
                            <td>
                                <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                City
                            </td>
                            <td>
                                <asp:TextBox ID="txtCity" CssClass="formtext_011" runat="server" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Pin Code
                            </td>
                            <td>
                                <asp:TextBox ID="txtPinCode" CssClass="formtext_011" runat="server" MaxLength="6"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtPinCode_FilteredTextBoxExtender1" runat="server"
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtPinCode">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                State
                            </td>
                            <td>
                                <asp:TextBox ID="txtState" CssClass="formtext_011" runat="server" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Landline No.
                            </td>
                            <td>
                                <asp:TextBox ID="txtLandlineNo" CssClass="formtext_011" runat="server" MaxLength="14"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Mobile No.
                            </td>
                            <td>
                                <asp:TextBox ID="txtMobileNo" CssClass="formtext_011" runat="server" MaxLength="10"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtMobileNo_FilteredTextBoxExtender1" runat="server"
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtMobileNo">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Date Of Birth
                            </td>
                            <td>
                                <span class="redtext">DD</span><asp:TextBox ID="txtDD" runat="server" Width="32px"
                                    MaxLength="2"></asp:TextBox><cc1:FilteredTextBoxExtender ID="txtDD_FilteredTextBoxExtender1" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="txtDD">
                                </cc1:FilteredTextBoxExtender>
                               <span class="redtext">MM</span> <asp:TextBox ID="txtMM" runat="server" Width="42px" MaxLength="2"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtMM_FilteredTextBoxExtender2" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="txtMM">
                                </cc1:FilteredTextBoxExtender>
                             <span class="redtext">YY</span>   <asp:TextBox ID="txtYY" runat="server" Width="47px" MaxLength="4"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtYY_FilteredTextBoxExtender3" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="txtYY">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Date Of Joining</td>
                            <td>
                                <span class="redtext">DD</span><asp:TextBox ID="txtDDD" runat="server" Width="32px"
                                    MaxLength="2"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtDDD_FilteredTextBoxExtender" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="txtDDD">
                                </cc1:FilteredTextBoxExtender>
                              <span class="redtext">MM</span>  <asp:TextBox ID="txtMMM" runat="server" Width="42px" MaxLength="2"> 
                    </asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtMMM_FilteredTextBoxExtender" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="txtMMM">
                                </cc1:FilteredTextBoxExtender>
                                <span class="redtext">YY</span><asp:TextBox ID="txtYYY" runat="server" Width="47px" MaxLength="4"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtYYY_FilteredTextBoxExtender" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="txtYYY">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="height: 31px; width: 42%;">
                                Religon
                            </td>
                            <td style="height: 31px">
                                <asp:TextBox ID="txtReligon" CssClass="formtext_011" runat="server" 
                                    MaxLength="10" Height="16px" Width="196px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Nationality
                            </td>
                            <td>
                                <asp:TextBox ID="txtNationality" CssClass="formtext_011" runat="server" 
                                    MaxLength="10" Height="16px" Width="190px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Department</td>
                            <td>
                                <asp:TextBox ID="txtDepartment" CssClass="formtext_011" runat="server" 
                                    MaxLength="10" Height="16px" Width="196px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Organizar Name</td>
                            <td>
                                <asp:DropDownList ID="Dprorg" runat="server" Height="19px" Width="196px">
                                    <asp:ListItem>AbhijeetSharma</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Type</td>
                            <td>
                                <asp:DropDownList ID="Dprtype" runat="server" Height="19px" Width="196px">
                                    <asp:ListItem>DSR</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Total Leave</td>
                            <td>
                                <asp:TextBox ID="txttotal" runat="server" Width="104px" MaxLength="4" 
                                    Height="16px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txttotal_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="txttotal">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 42%">
                                Designation</td>
                            <td>
                                <asp:TextBox ID="txtposit" CssClass="formtext_011" runat="server" 
                                    MaxLength="10" Height="16px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </fieldset> </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend>Education Qualification</legend>
                    <table style="height: 73px; width: 103%">
                        <tr>
                            <td class="txt">
                                Examination Passed
                            </td>
                            <td class="txt" style="width: 159px">
                                Board/University
                            </td>
                            <td class="txt">
                                Optional Subjects
                            </td>
                            
                            <td class="txt">
                                Percentage(%)
                            </td>
                            <td class="txt">
                                Division
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="height: 22px">
                                Secondary
                            </td>
                            <td style="width: 159px; height: 22px">
                                <asp:TextBox ID="txtS_BU" CssClass="formtext_01" runat="server" MaxLength="100"></asp:TextBox>
                            </td>
                            <td style="height: 22px">
                                <asp:TextBox ID="txtS_OS" CssClass="formtext_01" runat="server" MaxLength="100"></asp:TextBox>
                            </td>
                            <td style="height: 22px">
                                <asp:TextBox ID="txtS_Per" CssClass="formtext_01" runat="server" MaxLength="3"></asp:TextBox>
                            </td>
                            <td style="height: 22px">
                                <asp:TextBox ID="txtS_Div" CssClass="formtext_01" runat="server" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt">
                                Sr. Sec./Equivalent
                            </td>
                            <td style="width: 159px">
                                <asp:TextBox ID="txtSS_BU" CssClass="formtext_01" runat="server" MaxLength="100"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSS_OS" CssClass="formtext_01" runat="server" MaxLength="100"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSS_Per" CssClass="formtext_01" runat="server" MaxLength="3"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSS_Div" CssClass="formtext_01" runat="server" 
                                    MaxLength="10" Height="18px" Width="111px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
      
       
            
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend>Previous Jobs </legend>
                    <table width="100%">
                        <tr>
                            <td class="txt" style="height: 23px; width: 122px;">
                                Previous Job Name</td>
                            <td class="txt" style="height: 23px">
                                Work</td>
                            <td class="txt" style="height: 23px">
                                Position</td>
                                
                            <td class="txt" style="height: 23px; width: 166px;">
                                Mobile No</td>
                            <td class="txt" style="height: 23px">
                                Place</td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 122px">
                                &nbsp;</td>
                            <td>
                                <asp:TextBox ID="txtWork" CssClass="formtext_01" runat="server" MaxLength="100"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPosition" CssClass="formtext_01" Text="" runat="server" 
                                    MaxLength="100"></asp:TextBox>
                            </td>
                           
                            <td style="width: 166px">
                                <asp:TextBox ID="txtMob1" CssClass="formtext_01" runat="server" MaxLength="3" 
                                    Width="166px" ></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPlace1" CssClass="formtext_01" runat="server" 
                                    MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt" style="width: 122px">
                                &nbsp;</td>
                            <td>
                                <asp:TextBox ID="txtWork2" CssClass="formtext_01" runat="server" 
                                    MaxLength="100"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPosition2" CssClass="formtext_01" runat="server" 
                                    MaxLength="100"></asp:TextBox>
                            </td>
                            <td style="width: 166px">
                                <asp:TextBox ID="txtMobile2" CssClass="formtext_01" runat="server" 
                                    MaxLength="3" Height="17px" Width="164px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPlace2" CssClass="formtext_01" runat="server" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                
            </td>
        </tr>
      
        
    
        <tr>
            <td>
                <fieldset>
                    <legend>Reference </legend>
                    <table width="100%">
                        <tr>
                            <td class="txt" style="height: 23px">
                                &nbsp;</td>
                            <td class="txt" style="height: 23px">
                                Name</td>
                            <td class="txt" style="height: 23px">
                                &nbsp;Work</td>
                                
                            <td class="txt" style="height: 23px">
                                Mobile No</td>
                            <td class="txt" style="height: 23px">
                                Place</td>
                        </tr>
                        <tr>
                            <td class="txt">
                                &nbsp;</td>
                            <td>
                                <asp:TextBox ID="txtRName1" CssClass="formtext_01" runat="server" 
                                    MaxLength="100"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRWork1" CssClass="formtext_01" Text="" runat="server" 
                                    MaxLength="100"></asp:TextBox>
                            </td>
                           
                            <td>
                                <asp:TextBox ID="txtRMob1" CssClass="formtext_01" runat="server" MaxLength="3" 
                                    Width="170px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRPlace1" CssClass="formtext_01" runat="server" 
                                    MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt">
                                &nbsp;</td>
                            <td>
                                <asp:TextBox ID="txtRname2" CssClass="formtext_01" runat="server" 
                                    MaxLength="100"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRWork2" CssClass="formtext_01" runat="server" MaxLength="100"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRMob2" CssClass="formtext_01" runat="server" MaxLength="3" 
                                    Width="169px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRPlace2" CssClass="formtext_01" runat="server" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                   
            </td>
        </tr>
        <tr>
            
            <td align="center">  <asp:CheckBox ID="chkAgree" Checked="true" runat="server" CssClass="txt_new" Text="By submitting, I agree that all info entered was done accurately and truthfully.  " />
               </td>
        </tr>
        <tr>
            
            <td align="center">
                <asp:Button ID="btnSubmit" runat="server" Width="100px" Height="50px" Text="Submit"
                    OnClientClick="return CheckFieldValidation();" OnClick="btnSubmit_Click" />
                &nbsp;<asp:Button ID="btnReset" runat="server" Width="100px" Height="50px"
                    Text="Reset" OnClick="btnReset_Click" />
            &nbsp;<asp:Button ID="btnBack" runat="server" Width="100px" Height="50px"
                    Text="Back" OnClick="btnBack_Click" PostBackUrl="~/index.aspx" />
            </td>
        </tr>
       </table>
</asp:Content>



    