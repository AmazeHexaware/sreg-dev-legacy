﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_index : System.Web.UI.Page
{   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtPassword.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + btnLogin.UniqueID + "','')");
        }
    }
   
    protected void btnLogin_Click(object sender, EventArgs e)

    {
        string msg = string.Empty;
        try
        {
            System.Data.DataTable tbl = new System.Data.DataTable();
            tbl = SqlHelp.GetDataTable("Select adminID from admin where adminID='" + txtUserName.Text.Trim().Replace("'", "''") + "' and password='" + txtPassword.Text.Trim().Replace("'", "''") + "'");

            if (tbl.Rows.Count >= 1)
            {
                Session["adminID"] = tbl.Rows[0]["adminID"];
                Response.Redirect("dashboard.aspx");
            }

            else
            {
                lblMsg.Text = "Invalid User Name or Password";
                txtUserName.Text = "";
                txtPassword.Text = "";               
            }
        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
    }
   }
