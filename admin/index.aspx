﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="admin_index" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link href="css/Admin_style_3.css" rel="stylesheet" type="text/css"/>
    <link href="css/admin_style1.css" rel="stylesheet" type="text/css" />
    <title>Login</title>

    <script type="text/javascript">

        function CheckValidate() {

            if (trim(document.getElementById('txtUserName').value).length == 0 || trim(document.getElementById('txtUserName').value) == "User Name") {
                document.getElementById('<%=lblMsg.ClientID %>').innerHTML = "User Name Required";
                document.getElementById('txtUserName').focus();
                return false;
            }
            if (trim(document.getElementById('txtPassword').value).length == 0 || trim(document.getElementById('txtPassword').value) == "Password") {
                document.getElementById('<%=lblMsg.ClientID %>').innerHTML = "Password Required";
                document.getElementById('txtPassword').focus();
                return false;
            }
            return true;
        }

        function trim(str) {
            if (!str || typeof str != 'string')
                return null;

            return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
        }
    </script>

</head>
<body>
    <form id="formaction" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table border="0" align="center" cellpadding="0" cellspacing="0" 
        style="width: 923px">
        <tr>
            <td align="center" style="background-image: url('../images/screen1.jpg')">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Image ID="Imagelo" runat="server" Height="107px" 
                    ImageUrl="~/admin/images/logo.jpg" Width="971px" />
            </td>
        </tr>
        <tr>
            <td style="background-image: url('../images/screen1.jpg')">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td height="370" valign="top" background="images/login.jpg">
                <table border="0" align="center" cellpadding="0" cellspacing="6" 
                    style="width: 104%">
                    <tr>
                        <td height="50" colspan="2" align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td height="43" align="right" class="btn_06">
                            &nbsp;
                        </td>
                        <td align="left" class="title01">
                            Administrator Login
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="btn_06">
                            &nbsp;
                        </td>
                        <td align="left" class="btn_05">
                            <asp:Label ID="lblMsg" runat="server" BorderWidth="0px" CssClass="btn_05"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="32%" height="33" valign="top" 
                            style="color: #008000; font-size: 13pt; font-weight: bold;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            Admin Name</td>
                        <td width="68%" valign="top">
                            <asp:TextBox ID="txtUserName" runat="server" class="text" Style="font-family: verdana,helvetica,sans-serif;
                                font-size: 12px; width: 229px;" Height="22px" 
                                 Width="230px"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="txtUserName_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="txtUserName" WatermarkText="User Name">
                            </cc1:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td height="33" valign="top" 
                            style="color: #008000; font-size: 13pt; font-weight: bold;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            Password</td>
                        <td valign="top">
                            <asp:TextBox class="text" ID="txtPassword" TextMode="Password" runat="server" Style="font-family: verdana,helvetica,sans-serif;
                                font-size: 12px; width: 229px;"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="txtPassword_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="txtPassword" WatermarkText="Password">
                            </cc1:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td height="44" align="right">
                            &nbsp;
                        </td>
                        <td align="left" valign="middle">
                            <asp:Button ID="btnLogin" runat="server" Text="Enter" CssClass="btnText" OnClick="btnLogin_Click"
                                OnClientClick="return CheckValidate();" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="background-image: url('../images/screen1.jpg')">
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
