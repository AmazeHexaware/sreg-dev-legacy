﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Data;

/// <summary>
/// Summary description for GeneralUtility
/// </summary>
public class GeneralUtility
{
	public GeneralUtility()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    
    /// <summary>
    /// Create Image width:-125px and Height:-125px
    /// </summary>
    /// <param name="scaleFactor"></param>
    /// <param name="sourcePath"></param>
    /// <param name="targetPath"></param>

    public static void GenerateThumbnails_Small(double scaleFactor, Stream sourcePath, string targetPath)
    {

        using (var image = System.Drawing.Image.FromStream(sourcePath))
        {
            // can given width of image as we want

            if (image.Width > 120 && image.Height > 150)
            {
                var newWidth = (int)(120 * scaleFactor);
                // can given height of image as we want
                var newHeight = (int)(150 * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
            else
            {
                var thumbnailImg1 = new Bitmap(image.Width, image.Height);
                var thumbGraph1 = Graphics.FromImage(thumbnailImg1);
                thumbGraph1.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph1.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph1.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle1 = new Rectangle(0, 0, image.Width, image.Height);

                thumbGraph1.DrawImage(image, imageRectangle1);
                thumbnailImg1.Save(targetPath);
            }

        }


    }
    public static void BindCountry(DropDownList ddlCountry)
    {
        try
        {
            DataTable dtCountry = SqlHelp.GetDataTable("Select * from Country");
            if (dtCountry.Rows.Count > 0)
            {
                ddlCountry.DataSource = dtCountry;
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "Id";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, "Select Country");
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    public static string GetRandomPassword(int length)
    {
        char[] chars = "$%#@!*abcdefghijklmnopqrstuvwxyz1234567890?;:ABCDEFGHIJKLMNOPQRSTUVWXYZ^&".ToCharArray();
        string password = string.Empty;
        Random random = new Random();

        for (int i = 0; i < length; i++)
        {
            int x = random.Next(1, chars.Length);
            //Don't Allow Repetation of Characters
            if (!password.Contains(chars.GetValue(x).ToString()))
                password += chars.GetValue(x);
            else
                i--;
        }
        return password;
    }
    
}
