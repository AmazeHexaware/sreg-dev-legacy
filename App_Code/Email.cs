﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.IO;


/// <summary>
/// Summary description for Email
/// </summary>
public class Email
{
    public Email()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// This Function Send Email to Website Admin With Parameter Subject and MeassageBody
    /// </summary>
    /// <param name="strSubject"></param>
    /// <param name="msgBody"></param>
    /// <returns></returns>
    public static bool EmailToAdmin(string strSubject, string msgBody)
    {
        bool retVal = false;
        try
        {

            MailMessage message = new MailMessage("dsr@agritechexpo.com","dsr@agritechexpo.com", strSubject, msgBody);
            message.IsBodyHtml = true;
            System.Net.NetworkCredential Authentication = new System.Net.NetworkCredential("dsr@agritechexpo.com", "123agritech@@");
            SmtpClient client = new SmtpClient("agritechexpo.com");
            client.EnableSsl = false;
            client.UseDefaultCredentials = false;
            client.Credentials = Authentication;
            client.Send(message);
            retVal = true;                    
        }
        catch (Exception ex)
        {
            retVal = false;
            throw (ex);
        }
        return retVal;
    }
    /// <summary>
    /// This Function Send Email to User with Subject and MessageBody
    /// </summary>
    /// <param name="strEmail"></param>
    /// <param name="strSubject"></param>
    /// <param name="msgBody"></param>
    /// <returns></returns>
    
    public static bool EmailToUser(string strEmail, string msgBody, string subject)
    {
        bool retVal = false;
        try
        {
            MailMessage message = new MailMessage("dsr@agritechexpo.com", strEmail, subject, msgBody);
            message.IsBodyHtml = true;            
            System.Net.NetworkCredential Authentication = new System.Net.NetworkCredential("dsr@agritechexpo.com", "123agritech@@");
            SmtpClient client = new SmtpClient("agritechexpo.com");
            client.EnableSsl = false;
            client.UseDefaultCredentials = false;
            client.Credentials = Authentication;
            client.Send(message);
            retVal = true;
        }
        catch (Exception ex)
        {
            retVal = false;
            throw (ex);

        }
        return retVal;
    }


    /// This Function Sends Email for Password Forgot
    /// </summary>
    /// <param name="strEmail"></param>
    /// <param name="strSubject"></param>
    /// <param name="msgBody"></param>
    /// <returns></returns>
    public static bool ForgotPassword(string userid, string password, string emailaddress)
    {   
        bool retVal = false;
        try
        {
            string to, from, subject, body;
            to = emailaddress;
            from = "dsr@agritechexpo.com";
            subject = "Forgot Password";
            body = @"<table width='60%' align='left' cellpadding='0' cellspacing='5'>
                <tr>
                    <td colspan='2'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='font-family: Arial; font-weight: bold; font-size: 15px;'>
                        <span style='font-family: Verdana; font-size: small'>&nbsp;Hi,</span>
                    </td>
                </tr>
                <tr>
                    <td align='left' colspan='2' style='font-family: Arial; font-weight: normal; font-size: 14px;'>
                        <span style='font-weight: 700; font-size: small; font-family: Verdana'><span>&nbsp;Your
                            account information is :</span></span>
                    </td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='font-family: Arial; font-weight: normal; font-size: 15px;'>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan='2' align='left' valign='top'>
                        <table width='60%' cellspacing='5'>
                            <tr>
                                <td width='20%' align='left' valign='top' style='font-family: Arial; font-weight: normal;
                                    font-size: 13px;'>
                                    <span style='font-size: small; font-family: Verdana'>Email ID :</span>
                                </td>
                                <td width='80%' align='left' valign='top' style='font-family: Arial; font-weight: normal;
                                    font-size: 13px;'>
                                    <strong>" + emailaddress + @"</strong>
                                </td>
                            </tr>
                            <tr>
                                <td align='left' valign='top' style='font-family: Arial; font-weight: normal; font-size: 13px;'>
                                    <span style='font-family: Verdana; font-size: small'>Password :</span>
                                </td>
                                <td align='left' valign='top' style='font-family: Arial; font-weight: normal; font-size: 13px;'>
                                    <strong>" + password + @"</strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style='font-weight: 700; font-size: small; font-family: Verdana' align='left'
                        colspan='2'>
                        &nbsp;Sincerly,
                    </td>
                </tr>
                <tr>
                    <td style='font-weight: 700; font-size: small; font-family: Verdana' align='left'
                        colspan='2'>
                        &nbsp;<span style='color: #CC0000;'>Co-ordinator BBM & BTM</span>
                    </td>
                </tr>
                <tr>
                    <td style='font-weight: 700; font-size: small; font-family: Verdana' colspan='2'>
                        &nbsp;
                    </td>
                </tr>
            </table>";
            MailMessage message = new MailMessage(from, to, subject, body);
            message.IsBodyHtml = true;
            System.Net.NetworkCredential Authentication = new System.Net.NetworkCredential("dsr@agritechexpo.com", "123agritech@@");
            SmtpClient client = new SmtpClient("agritechexpo.com");
            client.EnableSsl = false;
            client.UseDefaultCredentials = false;
            client.Credentials = Authentication;
            client.Send(message);
            retVal = true;
        }
        catch (Exception ex)
        {
            retVal = false;
            throw (ex);
        }
        return retVal;
    }
}
