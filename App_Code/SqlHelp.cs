﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SqlHelp
/// </summary>
public class SqlHelp
{
    public SqlHelp()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// Private function For Get Connection string from WebConfig file
    /// </summary>
    /// <returns></returns>
    private static string GetConnectionString()
    {
        string strConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ToString();
        return strConnectionString;
    }
    /// <summary>
    /// public function for get datatable from Database form Query
    /// </summary>
    /// <param name="strQry"></param>
    /// <returns></returns>
    public static DataTable GetDataTable(string strQry)
    {

        DataTable dt = new DataTable();
        SqlConnection sCon = new SqlConnection(SqlHelp.GetConnectionString());
        
        
            sCon.Open();
            SqlDataAdapter sAdp = new SqlDataAdapter(strQry, sCon);
            sAdp.Fill(dt);
        
     
        return dt;

    }
    /// <summary>
    /// public function for getdatareader form database from query(This is fast from datatable).
    /// </summary>
    /// <param name="strQry"></param>
    /// <returns></returns>
    public static SqlDataReader GetDataReader(string strQry)
    {
        SqlConnection sCon = new SqlConnection(SqlHelp.GetConnectionString());
        SqlCommand cmd = new SqlCommand(strQry, sCon);
        sCon.Open();
        SqlDataReader sdr = cmd.ExecuteReader();
        return sdr;

    }
    /// <summary>
    /// Public function for ExcecuteNonQuery(Insert,Update,Delete) in Database.
    /// </summary>
    /// <param name="strQry"></param>
    /// <returns></returns>
    public static bool ForExecuteNonQuery(string strQry)
    {
        bool retVal = false;
        SqlConnection sCon = new SqlConnection(SqlHelp.GetConnectionString());
        SqlCommand sCmd = new SqlCommand(strQry, sCon);
        try
        {
            sCon.Open();
            if (sCmd.ExecuteNonQuery() > 0)
            {
                retVal = true;
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        finally
        {
            sCon.Dispose();
        }
        return retVal;
    }
    /// <summary>
    /// Public function for getsinglevalue from database table.
    /// </summary>
    /// <param name="strQry"></param>
    /// <returns></returns>
    public static object GetSingleValue(string strQry)
    {
        SqlConnection sCon = new SqlConnection(SqlHelp.GetConnectionString());
        SqlCommand sCmd = new SqlCommand(strQry, sCon);
        object obj;
        try
        {
            sCon.Open();
            obj = sCmd.ExecuteScalar();
        }
        catch (Exception ex)
        {
            obj = -1;
            throw (ex);
        }
        finally
        {
            sCon.Dispose();
        }
        return obj;
    }

}
